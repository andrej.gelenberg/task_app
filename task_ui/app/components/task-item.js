import Component from '@ember/component';
import { computed } from '@ember/object';

export default Component.extend({
  item:    null,
  classNames: ["box"],
  del() {},

  isToDo: computed('item.state', function() {
    return this.item.state == 'todo';
  }),

  isInProgress: computed('item.state', function() {
    return this.item.state == 'in_progress';
  }),

  isDone: computed('item.state', function() {
    return this.item.state == 'done';
  }),

  actions: {
    setToDo() {
      this.item.set('state', 'todo');
      this.item.save()
    },

    setInProgress() {
      this.item.set('state', "in_progress");
      this.item.save()
    },

    setDone() {
      this.item.set('state', "done");
      this.item.save()
    },

    del() {
      this.del(this.item);
    }

  }

});
