import Controller from '@ember/controller';

export default Controller.extend({
  new_title: "",
  new_description: "",

  actions: {
    del(item) {
      item.destroyRecord();
      console.log(this.transitionTo("index"));
    },
    new() {
      let task = this.store.createRecord('task', {
        title: this.new_title,
        description: this.new_description,
        state: 'todo'
      });
      let t = this;
      task.save().then(function(){ t.trantisionTo('index'); });
    }
  }
});
