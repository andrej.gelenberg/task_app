def db_init(db)
  db.execute 'CREATE TABLE IF NOT EXISTS task (id INTEGER PRIMARY KEY NOT NULL, title TEXT NOT NULL, description TEXT, state TEXT NOT NULL);'
end

def task(id, title, descritpion, state)
  {type: "tasks", id: id, attributes: {title: title, description: descritpion, state: state} }
end
