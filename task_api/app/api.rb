require 'sinatra'
require 'sqlite3'
require 'json'
require_relative 'db'

set :port, 8080
set :bind, '0.0.0.0'
set :logging, true

db = SQLite3::Database.new "/data/task.db"
db_init db

def json_api(data, code = 200)
  [code, {"Content-Type" => "application/vnd.api+json"}, JSON.generate({data: data})]
end

get '/' do
  [200, 'task app api service']
end

get '/1/tasks' do
  data = []
  db.execute 'SELECT id, title, description, state FROM task' do |row|
    data << task(*row)
  end
  json_api(data)
end

get '/1/tasks/:id' do |id|
  data = []
  db.execute 'SELECT id, title, description, state FROM task WHERE id = ?', id.to_i do |row|
    data << task(*row)
  end
  json_api data
end

patch '/1/tasks/:id' do |id|
  b = JSON.load request.body
  b = b['data']
  b = b[0] if b.is_a? Array
  return [400, 'invalid data type'] if b['type'] != 'tasks'
  as = b['attributes']
  db.execute 'UPDATE task SET title = ?, description = ?, state = ? WHERE id = ?', [as['title'], as['description'], as['state'], b['id'].to_i]
  [200]
end

delete '/1/tasks/:id' do |id|
  db.execute 'DELETE FROM task WHERE id = ?', [id.to_i]
  [200, '{"meta": {}}']
end

post '/1/tasks' do
  b = JSON.load request.body
  b = b['data']
  b = b[0] if b.is_a? Array
  return [400, 'invalid data type'] if b['type'] != 'tasks'
  as = b['attributes']
  id = 0
  db.execute 'INSERT INTO task (title, description, state) VALUES (?, ?, ?)', [as['title'], as['descritpion'], as['state']]
  db.execute 'SELECT last_insert_rowid()' do |row|
    id = row[0]
  end
  json_api(task(id, as['title'], as['description'], as['state']), 201)
end
